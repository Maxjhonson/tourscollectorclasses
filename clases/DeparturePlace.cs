﻿using System;

namespace clases
{
    public class DeparturePlace:Place
    { 
        public TimeSpan TimeOfDeparture { get; set; }
    }
}