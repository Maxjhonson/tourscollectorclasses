﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clases
{
    public class Tour
    {

         
        public List<string> ImagsUrls { get; set; }
        public string TourTitle { get; set; }
        public Company Company { get; set; }
        public List<Place> PlacesToVisit { get; set; }
        public List<string> Include { get; set; }
        public DateTime DepartureDate { get; set; }
        public List<DeparturePlace> DeparturePlaces { get; set; }
        public decimal AdultPrice { get; set; }
        public decimal KidsPrice { get; set; }
        public List<string> RecommendItems { get; set; }
        public TipeOfTour TipeOfTour { get; set; }
        public decimal CouplePrice { get; set; }

    }
}
