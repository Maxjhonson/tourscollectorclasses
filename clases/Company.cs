﻿namespace clases
{
    public class Company
    {
        public string Name { get; set; }
        public string Slogan { get; set; }
        public decimal RateName { get; set; }
        public string RateLetter { get; set; }
        public string LogoUrl { get; set; }
        public string PhoneNumber { get; set; }
        public string WhatsApp { get; set; }
        public string Instagram { get; set; }


    }
}